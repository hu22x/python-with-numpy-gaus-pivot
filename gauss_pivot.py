import os
import argparse
import numpy as np

def confirmMenu():

    vm = input("Try an0ther matrix (Y/N): ")

    if vm.lower() == "y":
        gppMethod()
    else :        
        print("Bye ...")
        os.system("clear")

def setMatrix(row,col):
    mt = np.eye(row, col)

    v = 0;

    for m in range(0, row):
        for n in range(0, col):
            it = input("matrix (" + str(m) + ", " + str(n) + ") = ")
            
            if it == "":
                it = 0

            mt.itemset((m, n), float(it))
            v+=1

    return mt


def gppMethod():
    os.system("clear")
    print("proses GPP method.")
    x_matrix  = input("Dimension of matrix :")

    _x           = int(x_matrix)
    _y           = int(x_matrix)

    print("\n")
    print("Prepare to set matrix")
    _matrix   = setMatrix(_x, _y)

    print("\n")
    print("Prepare to value matrix")
    _v_matrix = setMatrix(_y, 1)

    print("\nmain matrix is ")
    print(_matrix)

    print(" ")

    print("value matrix is ")
    print(_v_matrix)

    _matrix_copy = np.copy(_matrix)
    _matrix_diag = np.diag(_matrix)

    for i in range(0, _x - 1 ):         # start loop diagonal matrix
        for j in range(0, _x):
            if j <= i:
                # continue to next loop
                continue

            x_pivot = _matrix_copy.item(i,i)

            for k in range(0, _y):
                if k == i:                    
                    _row_value = _matrix_copy.item(j,k)
                    _matrix_copy.itemset((j,k), 0)

                else:
                    _item_matrix = (x_pivot * _matrix_copy.item(j,k)) - (_row_value * _matrix_copy.item(i,k))                    
                    _matrix_copy.itemset((j,k), _item_matrix)

            _item_v_matrix = (x_pivot * _v_matrix.item(j,0)) - (_row_value * _v_matrix.item(i,0))
            _v_matrix.itemset((j,0), _item_v_matrix)

            print("\nmatrix value")
            print(_matrix_copy)
            print("")
            print("matrix vector value")
            print( _v_matrix)

    print("\nTry to find the value of variable (x1, x2, x3, ..., xn) \n")

    x_value = 0
    x       = {}
    _n      = _x - 1

    while _n >= 0:
        x_value = 0
        _m      = _y - 1
        while _m >= 0:
            if _m == _n:
                # x[]
                print("get value x[", _n, "] = (", _v_matrix.item(_n, 0), "-", x_value, ")", "/", _matrix_copy.item(_n, _m))
                x[_m] = (_v_matrix.item(_n, 0) - x_value ) / _matrix_copy.item(_n, _m) 
                break
            x_value += (_matrix_copy.item(_n, _m) * x[_m]) 
            _m -= 1
        _n -= 1

    print("value x = ", x)
    confirmMenu()

gppMethod()

